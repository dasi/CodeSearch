@echo off

set BUILD_DIR=build_%1

if not exist %BUILD_DIR% mkdir %BUILD_DIR%

set XD=
if %1 == Debug set XD=D

set QT_PATH=C:\nph\Qt\5.12.0\msvc2017_64

cd %BUILD_DIR%
cmake -G "Visual Studio 15 2017 Win64" -DCMAKE_PREFIX_PATH=%QT_PATH% -DCMAKE_BUILD_TYPE=%1 %2 %3 %4 %5 %6 %7 %8 %9 ..
if errorlevel 1 goto exit
cmake --build . --config %1
if errorlevel 1 goto exit
cpack -G NSIS .

:exit
cd ..
echo 'bye...'
