function Component()
{
    // default constructor
}

Component.prototype.createOperations = function()
{
    component.createOperations();
    if (systemInfo.productType === "windows") {
        component.addOperation("CreateShortcut", "@TargetDir@/bin/CodeSearch.exe", "@StartMenuDir@/CodeSearch.lnk",
        "workingDirectory=@TargetDir@", "iconPath=@TargetDir@/bin/CodeSearch.exe",
        "iconId=0");
    }
}
