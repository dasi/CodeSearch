#include "settings.h"
#include "ui_settings.h"

#include <QDir>
#include <QCloseEvent>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QTextStream>
#include <QFileDialog>

#include "paths.hpp"

struct SettingsPrivate {
    SettingsModel model;
};

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings),
    d(new SettingsPrivate())
{
    ui->setupUi(this);

    connect(ui->btBrowse, &QPushButton::clicked, [this]() {

        QString fileName = QFileDialog::getOpenFileName(this, tr("Select editor"),
                                                         QDir::homePath(),
                                                         tr("Programs (*.png *.lnk)"));
        if ( !fileName.isNull() && !fileName.isEmpty() ) {
            ui->leEditorPath->setText(fileName);
        }

    });

    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    connect(ui->buttonBox, &QDialogButtonBox::accepted, [this]() {
        saveSettings();
        accept();
    });

    connect(ui->leEditorPath, &QLineEdit::textChanged, [this]() {
       d->model.editor.path = ui->leEditorPath->text();
    });

    connect(ui->leEditorArgs, &QLineEdit::textChanged, [this]() {
       d->model.editor.args = ui->leEditorArgs->text();
    });

    loadSettings();
}

SettingsModel const & Settings::model() const
{
    return d->model;
}

void Settings::closeEvent(QCloseEvent * e)
{

}

bool Settings::loadSettings()
{
    auto file_ = paths::settings_file();
    if ( !file_.exists() )
        return true;

    QJsonObject root;
    {
        QFile data(file_.absoluteFilePath());
        if ( !data.open(QFile::ReadOnly) )
            return false;

        QTextStream in(&data);
        QJsonDocument doc = QJsonDocument::fromJson(in.readAll().toUtf8());
        root = doc.object();
    }

    if ( root.contains("editor") ) {

        QJsonObject obj = root["editor"].toObject();
        d->model.editor.path = obj["path"].toString();
        d->model.editor.args = obj["args"].toString();

        // update view
        // FIXME, move into a function
        ui->leEditorPath->setText(d->model.editor.path);
        ui->leEditorArgs->setText(d->model.editor.args);
    }

    // load excluded patterns
    auto exclude_file = paths::app_storage().absoluteFilePath("ignore");
    QFile xf(exclude_file);
    if ( xf.open(QFile::ReadOnly) ) {
        QTextStream xfts(&xf);
        ui->txExcluded->appendPlainText(xfts.readAll());
    } else {
        ui->txExcluded->appendPlainText(
                    ".vscode\n"
                    ".git\n"
                    ".svn\n"
                    "*.contrib\n"
                    "*.keep\n"
                    "*.contrib.?\n"
                    "*.keep.?");
    }
    return true;
}

bool Settings::saveSettings()
{
    QJsonObject root;
    QJsonObject editor;
    editor["path"] = QJsonValue(d->model.editor.path);
    editor["args"] = QJsonValue(d->model.editor.args);

    root["editor"] = editor;

    {
        auto file_ = paths::settings_file();
        QFile data(file_.absoluteFilePath());
        if ( !data.open(QFile::WriteOnly | QFile::Truncate) )
            return false;
        QTextStream out(&data);
        QJsonDocument doc(root);
        out << doc.toJson();
    }

    // save excluded patterns
    auto exclude_file = paths::app_storage().absoluteFilePath("ignore");
    QFile xf(exclude_file);
    //QTextStream xfts(&xf);
    if ( xf.open(QFile::WriteOnly) )
        xf.write(ui->txExcluded->toPlainText().toUtf8());
    else
        return false;

    return true;
}

Settings::~Settings()
{
    delete ui;
}
