#ifndef QRESULTVIEWMODEL_H
#define QRESULTVIEWMODEL_H

#include <QAbstractTableModel>
#include <QFileInfo>
#include <QFileIconProvider>

struct ResultRecord {
    QFileInfo file;
    quint32 rowNr;
};
Q_DECLARE_METATYPE(ResultRecord)

class QResultViewModel : public QAbstractTableModel
{
public:
    QResultViewModel();
    QResultViewModel(QList<ResultRecord> const & result);

    // QAbstractTableModel overloads
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    // add items
    void merge(QList<ResultRecord> const & items);
private:
    QList<ResultRecord> _result;
    QFileIconProvider _fileIconProvider;
};

#endif // QRESULTVIEWMODEL_H
