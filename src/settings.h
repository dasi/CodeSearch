#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QScopedPointer>

namespace Ui {
class Settings;
}

struct SettingsModel {
    struct Editor {
        QString path;
        QString args;
    } editor;
};

struct SettingsPrivate;
class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = 0);
    ~Settings();

    SettingsModel const & model() const;

protected:
    void closeEvent(QCloseEvent *);
private:
    bool saveSettings();
    bool loadSettings();
private:
    Ui::Settings *ui;    
    QScopedPointer<SettingsPrivate> d;
};

#endif // SETTINGS_H
