#include "indexer.h"
#include "paths.hpp"

#include <QFileInfo>
#include <QProcess>
#include <QDebug>

const char* CINDEX_PATH = "cindex";

Indexer::Indexer(QString const& path)
    : path_(path), name_(QFileInfo(path).fileName())
{

}

void Indexer::listPaths()
{
    paths_.clear();

    auto args = QStringList() << "-indexpath" << path_ << "-list";
    auto proc_ = new QProcess();
    void (QProcess::* finishedPtr)(int ec) = &QProcess::finished;
    connect(proc_, finishedPtr, [=](int ec) {
        proc_->deleteLater();

        if ( ec == 0 ) {
            foreach(QByteArray b, proc_->readAllStandardOutput().split('\n')) {
                QString path_ = QString::fromUtf8(b.trimmed());
                if ( !path_.isEmpty() )
                    paths_.append(path_);
            }
        }

        emit completed(name_);
    });

    proc_->start(CINDEX_PATH, args);
}

void Indexer::create(QStringList const &ixPaths)
{
    paths_.clear();

    auto exclude_file = paths::app_storage().absoluteFilePath("ignore");
    auto args = QStringList() << "-indexpath" << path_ << "-reset";
    if ( QFile(exclude_file).exists() )
        args << "-exclude" << exclude_file;
    if ( !ixPaths.isEmpty() )
         args << ixPaths;

    qDebug() << "running: " << CINDEX_PATH << " " << args;

    auto proc_ = new QProcess();
    void (QProcess::* finishedPtr)(int ec) = &QProcess::finished;
    connect(proc_, finishedPtr, [=](int ec) {
        proc_->deleteLater();
        if ( !ec && !ixPaths.isEmpty() )
            paths_.append(ixPaths);
        emit completed(name_);
    });

    proc_->start(CINDEX_PATH, args);
}

void Indexer::update()
{
    update(QStringList());
}

void Indexer::update(QStringList const &ixPaths)
{
    auto exclude_file = paths::app_storage().absoluteFilePath("ignore");
    auto args = QStringList() << "-indexpath" << path_;
    if ( !ixPaths.isEmpty() )
         args << ixPaths;
    if ( QFile(exclude_file).exists() )
        args << "-exclude" << exclude_file;

    qDebug() << "running: " << CINDEX_PATH << " " << args;

    auto proc_ = new QProcess();
    void (QProcess::* finishedPtr)(int ec) = &QProcess::finished;
    connect(proc_, finishedPtr, [=](int ec) {
        proc_->deleteLater();
        if ( !ec && !ixPaths.isEmpty() )
            paths_.append(ixPaths);
        emit completed(name_);
    });

    proc_->start(CINDEX_PATH, args);
}

void Indexer::remove()
{
    auto args = QStringList() << "-indexpath" << path_ << "-reset";
    auto proc_ = new QProcess();
    void (QProcess::* finishedPtr)(int ec) = &QProcess::finished;
    connect(proc_, finishedPtr, [=]() {
        proc_->deleteLater();
        emit completed(name_);
    });

    proc_->start(CINDEX_PATH, args);
    paths_.clear();
}
