#include <QtGlobal>
#if defined(Q_OS_WIN)
#    include "qt_windows.h"
#endif
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QSettings>
#include <QToolTip>
#include <QProcess>
#include <QMessageBox>
#include <QTextStream>
#include <QFontDatabase>

#include "about.h"
#include "indexer.h"
#include "indexedit.h"
#include "lexer-chooser.h"
#include "paths.hpp"
#include "searchengine.h"
#include "settings.h"

namespace fonts {
    const QFont monospace() {
        int pointSize = 9;
#if defined(Q_OS_WIN)
        QFont f("Consolas");
#else
        QFont f = QFontDatabase::systemFont(QFontDatabase::FixedFont);
#endif
        f.setPointSize(pointSize);
        return f;
    }
}

#if defined(Q_OS_WIN)
static void runProgram(QString path, QString args, bool translateSeparators) {

    QString filePath = translateSeparators ? QDir::toNativeSeparators(path) : path;
    SHELLEXECUTEINFO ShExecInfo;

    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_FLAG_NO_UI;
    ShExecInfo.fMask = NULL;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = (LPCWSTR)(filePath.utf16());
    ShExecInfo.lpParameters = args.isEmpty() ? NULL : (LPCWSTR)(args.utf16());
    //ShExecInfo.lpDirectory = static_cast<LPCTSTR>(directory.utf16());
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_NORMAL;
    ShExecInfo.hInstApp = NULL;

    ShellExecuteExW(&ShExecInfo);
}
#else
static void runProgram(QString path, QString args, bool translateSeparators) {

    QString filePath = translateSeparators ? QDir::toNativeSeparators(path.trimmed()) : path.trimmed();

    filePath.replace("\"", "\\\"");
    args    .replace("\"", "\\\"");

    QProcess::startDetached("sh -c \"" + filePath + " " + args.trimmed() + "\"");
}
#endif

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    jobManager(this)
{
    ui->setupUi(this);
    ui->preview->setReadOnly(true);
    ui->preview->setFont(fonts::monospace());

    // Due to https://bugreports.qt.io/browse/QTBUG-32981
    // I cannot enable sorting on the tableview
    // because using QSortProxyModel causes often crashes
    ui->resultView->setModifier( Qt::ShiftModifier );
    ui->resultView->setSortingEnabled(true);
    ui->resultView->setSortIcons(
                QIcon(":/icons/bullet_arrow_up.png"),
                QIcon(":/icons/bullet_arrow_down.png"));
    ui->resultView->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->resultView->setSelectionBehavior(QAbstractItemView::SelectRows);

    markerNr = ui->preview->markerDefine(QsciScintilla::Background);
    ui->preview->setMarkerBackgroundColor(QColor("#FFF59D"), markerNr);

    indicatorNr = ui->preview->indicatorDefine(QsciScintilla::FullBoxIndicator);
    ui->preview->setIndicatorForegroundColor(QColor::fromRgba(qRgba(0xFF, 0x12, 0x12, 0x7F)), indicatorNr);

    // set margin for line numbers
    auto editor_font = ui->preview->font();
    auto font_metrics = ui->preview->fontMetrics();
    ui->preview->setMarginWidth(0, font_metrics.width("00000")+6);
    ui->preview->setMarginLineNumbers(0, true);
    ui->preview->setMarginsBackgroundColor(QColor("#E0E0E0"));

    connect(ui->leSearch, SIGNAL(returnPressed()), SLOT(searchCode()));
    connect(ui->lePathFilter, SIGNAL(returnPressed()), SLOT(searchCode()));
    connect(ui->sbMaxFileRes, SIGNAL(editingFinished()), SLOT(searchCode()));
    connect(ui->sbMaxRes, SIGNAL(editingFinished()), SLOT(searchCode()));


    // preload lexers
    permute::LexerChooser::Ref();

    loadIndices();

    connect(ui->actionSettings, &QAction::triggered, [this](){
       Settings cfgUI;
       cfgUI.exec();

       cfg = cfgUI.model();
    });

    connect(ui->resultView, SIGNAL(activated(QModelIndex)), SLOT(openInEditor(QModelIndex)));
    connect(ui->preview, SIGNAL(SCN_DOUBLECLICK(int,int,int)), SLOT(openInEditor()));

    // status bar
    jobCount = new QLabel(this);
    ui->statusBar->addPermanentWidget(jobCount);

    const QString IDLE_MESSAGE = tr("IDLE.");

    ui->statusBar->showMessage(IDLE_MESSAGE);

    connect(ui->tbRefresh, &QToolButton::clicked, [this]() {
        QString ixPath = ui->cbIndex->currentData().toString();
        QString ixName = ui->cbIndex->currentText();
        QString jobName = QString(tr("Updating index %1")).arg(ixName);

        jobManager.push(jobName, [this, ixPath](JobManager* jobM) {
            auto* ix = new Indexer(ixPath);
            QObject::connect(ix, SIGNAL(completed(QString)), jobM, SLOT(finalizeCurrentJob()));
            QObject::connect(ix, SIGNAL(completed(QString)), ix, SLOT(deleteLater()));
            ix->update();
        });
    });

    connect(ui->tbDelete, &QToolButton::clicked, [this]() {
        QString ixPath = ui->cbIndex->currentData().toString();
        QString ixName = ui->cbIndex->currentText();
        QString jobName = QString(tr("Removing index %1")).arg(ixName);
        jobManager.push(jobName, [this, ixPath](JobManager* jobM) {
            auto* ix = new Indexer(ixPath);
            QObject::connect(ix, SIGNAL(completed(QString)), jobM, SLOT(finalizeCurrentJob()));
            QObject::connect(ix, SIGNAL(completed(QString)), ix, SLOT(deleteLater()));
            ix->remove();
        });
    });

    connect(ui->tbCreate, &QToolButton::clicked, [this]() {
       IndexEdit dlg(jobManager, this);
       dlg.exec();
    });

    connect(ui->tbEdit, &QToolButton::clicked, [this]() {
       IndexEdit dlg(jobManager, this);
       dlg.setEditMode(ui->cbIndex->currentData().toString());
       dlg.exec();
    });

    connect(&jobManager, &JobManager::jobStarted, [this](auto name) {
        ui->statusBar->showMessage( QString("%1...").arg(name) );
    });

    connect(&jobManager, SIGNAL(jobAdded()), this, SLOT(displayJobCount()));
    connect(&jobManager, SIGNAL(jobCompleted()), this, SLOT(displayJobCount()));

    connect(&jobManager, &JobManager::jobCompleted, [IDLE_MESSAGE, this]() {
        if ( jobManager.isEmpty() )
            ui->statusBar->showMessage( IDLE_MESSAGE );
        else
            jobManager.runNext();

        auto wasSelected = ui->cbIndex->currentData();
        loadIndices();
        for(int i = 0; i < ui->cbIndex->count(); i++) {
            if ( ui->cbIndex->itemData(i) == wasSelected )
            {
                ui->cbIndex->setCurrentIndex(i);
                break;
            }
        }
    });

    connect(ui->actionE_xit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));
    connect(ui->actionAbout_QT, SIGNAL(triggered(bool)), qApp, SLOT(aboutQt()));
    connect(ui->actionAbout_CodeSearch, &QAction::triggered, [this]() {
        About ab(this);
        ab.exec();
    });

    setWindowTitle(tr("Code search"));

    // load settings
    Settings cfgUI;
    cfg = cfgUI.model();

    // set focus to the search bar
    ui->leSearch->setFocus();

    applyStylesheet();
    connect(ui->actionReload_Theme, SIGNAL(triggered(bool)), SLOT(applyStylesheet()));

    // restore layout settings
    QSettings settings(paths::app_storage().absoluteFilePath("layout.ini"), QSettings::IniFormat);
    restoreState(settings.value("mainWindow_state").toByteArray());
    restoreGeometry(settings.value("mainWindow_geometry").toByteArray());
    ui->splitter->restoreState(settings.value("splitter_1_state").toByteArray());
    ui->splitter_2->restoreState(settings.value("splitter_2_state").toByteArray());
}

void MainWindow::closeEvent(QCloseEvent* ev)
{
    QSettings settings(paths::app_storage().absoluteFilePath("layout.ini"), QSettings::IniFormat);
    settings.setValue("mainWindow_state", saveState());
    settings.setValue("mainWindow_geometry", saveGeometry());
    settings.setValue("splitter_1_state", ui->splitter->saveState());
    settings.setValue("splitter_2_state", ui->splitter_2->saveState());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::searchCode()
{
    if ( ui->leSearch->text().length() < 3 )  {
        QToolTip::showText(
                    ui->leSearch->mapToGlobal(ui->leSearch->pos()),
                    tr("<p style='color:#ff0000'>Please type <em>at least</em> 3 characters</p>"),
                    this,
                    this->rect(),
                    2000);

    } else {
        auto args = SearchEngine::SearchArguments {
            ui->cbIndex->currentData().toString(),
            ui->leSearch->text(),
            ui->lePathFilter->text(),
            ui->ckCaseIns->isChecked(),
            ui->sbMaxRes->value(),
            ui->sbMaxFileRes->value()
        };

        auto searcher = new SearchEngine();

        connect(searcher, &SearchEngine::resultsReady, [this](QList<ResultRecord> list) {
            static_cast<QResultViewModel&>(*resultModel).merge(list);
            ui->resultView->setModel(resultModel.data());

            connect(ui->resultView->selectionModel(), &QItemSelectionModel::currentRowChanged,
            [this](const QModelIndex &current, const QModelIndex &previous) {
                Q_UNUSED(previous)
                // retrieve model from the view
                auto model = qvariant_cast<ResultRecord>(ui->resultView->model()->data(current, Qt::UserRole));
                loadFile(model);
            });

        });

        connect(searcher, SIGNAL(completed()), searcher, SLOT(deleteLater()));

        resultModel.reset(new QResultViewModel());
        ui->resultView->setModel(resultModel.data());
        ui->resultView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
        ui->resultView->horizontalHeader()->setStretchLastSection(true);

        searchArgs = args;

        searcher->search(args);
    }
}

void MainWindow::loadFile(ResultRecord const & item)
{
    auto fileName = item.file.absoluteFilePath();
    auto lineNr = item.rowNr - 1;

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    QTextStream in(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    ui->preview->setText(in.readAll());

    auto lexer = permute::LexerChooser::Ref().NewLexerBySuffix(ui->preview, item.file.suffix());
    if ( nullptr != lexer ) {
        lexer->setFont(fonts::monospace());
        ui->preview->setLexer(lexer);
    } else {
        ui->preview->setLexer(nullptr);
        ui->preview->setFont(fonts::monospace());
    }

    qDebug() << "find " << searchArgs.searchRx << " from line " << item.rowNr;

    if ( ui->preview->findFirst(
                searchArgs.searchRx,
                true,                        // regular expression
                !searchArgs.caseInsensitive, // case sensitive
                false,                       // word only
                false,                       // wrap
                true,                        // forward
                item.rowNr-1, 0,             // line, index
                true,                        // show
                false) ) {                   // posix regex
        int lineFrom, indexFrom, lineTo, indexTo;
        ui->preview->getSelection(&lineFrom, &indexFrom, &lineTo, &indexTo);
        ui->preview->setSelection(0, 0, 0, 0);


        qDebug() << "found at position " << lineFrom << ", " << indexFrom;

        // highlight text
        ui->preview->fillIndicatorRange(lineFrom, indexFrom, lineTo, indexTo, indicatorNr);
    }

    ui->preview->ensureLineVisible(lineNr);
    ui->preview->markerAdd(lineNr, markerNr);

    QApplication::restoreOverrideCursor();
}

void MainWindow::loadIndices()
{
    ui->cbIndex->clear();

    auto index_dir = paths::index_storage();
    for(auto index_file: index_dir.entryInfoList() ) {
        if ( index_file.isFile() )
            ui->cbIndex->addItem(index_file.fileName(), index_file.filePath());
    }
}

void MainWindow::displayJobCount()
{
    auto count_ = QString("%1 queued jobs").arg(jobManager.count());
    jobCount->setText(count_);
}

void MainWindow::openInEditor()
{
    openInEditor(ui->resultView->currentIndex());
}

void MainWindow::openInEditor(QModelIndex index)
{
    ResultRecord item_ = qvariant_cast<ResultRecord>(
                ui->resultView->model()->data(index, Qt::UserRole));

    QString args = cfg.editor.args;
    args.replace("$file", item_.file.absoluteFilePath())
        .replace("$line", QString::number(item_.rowNr));

    runProgram(cfg.editor.path, args, true);

    qDebug() << "launch: " << cfg.editor.path << " + " << args;
}

void MainWindow::applyStylesheet()
{
    auto old_cwd = QDir::currentPath();
    QFile theme_file(paths::theme_directory().absoluteFilePath("theme.qss"));
    QDir::setCurrent(paths::theme_directory().absolutePath());
    if ( theme_file.exists() && theme_file.open(QFile::ReadOnly) )
        setStyleSheet(QString::fromUtf8(theme_file.readAll()));

    QTimer *t = new QTimer(this);
    connect(t, &QTimer::timeout, [t, old_cwd]() {
        QDir::setCurrent(old_cwd);
        t->deleteLater();
    });
}
