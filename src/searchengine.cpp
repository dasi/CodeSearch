#include "searchengine.h"
#include <QDebug>

const char* CSEARCH_PATH = "csearch";

SearchEngine::SearchEngine(QObject *parent) : QObject(parent)
{
    connect(&searcher, SIGNAL(readyReadStandardOutput()), this, SLOT(parseCSearchResult()));
    connect(&searcher, SIGNAL(finished(int)), this, SIGNAL(completed()));
}

void SearchEngine::search(SearchArguments const & args)
{
    auto cmdArgs = QStringList() << "-indexpath" << args.indexPath << "-n";
    if ( args.caseInsensitive )
        cmdArgs << "-i";
    if ( !args.pathRx.isNull() && !args.pathRx.isEmpty() )
        cmdArgs << "-f" << args.pathRx;
    if ( args.maxResults > 0 )
        cmdArgs << "-m" << QString::number(args.maxResults);
    if ( args.maxResultPerFile > 0 )
        cmdArgs << "-M" << QString::number(args.maxResults);
    cmdArgs << args.searchRx;

    spawnCSearch(cmdArgs);
}

void SearchEngine::spawnCSearch(const QStringList &args)
{
    searcher.start(CSEARCH_PATH, args);
    qDebug() << "start search with args: " << args;
}

void SearchEngine::parseCSearchResult()
{
    QStringList lines;
    char buf[0x10000];
    for(;;) {
        qint64 lineLength = searcher.readLine(buf, sizeof(buf));
        if ( lineLength <= 0 ) break;
        lines << QString::fromUtf8(buf);
    }

    QList<ResultRecord> output;
    QRegExp rx("([^\\0]+):(\\d+):.*");

    std::transform(lines.begin(), lines.end(), std::back_inserter(output), [&](auto& str) -> ResultRecord {
        //auto str = QString::fromUtf8(line_);
        QStringList list;

        // <path>:<line>:<text>

        // check if the first two characters are
        // a windows drive
        QChar sep(':');
        bool starts_with_drive = str[1] == sep && str[0].isLetter();

        int start_pos = starts_with_drive ? 2 : 0;

        // find tokens
        auto o_path = str.indexOf(sep, start_pos);
        auto o_line = str.indexOf(sep, o_path+1);

        auto path_ = str.left(o_path);
        auto line_nr = str.mid(o_path+1, o_line-o_path-1);

        return { QFileInfo(path_), static_cast<quint32>(line_nr.toInt()) };
    });

    if ( !output.isEmpty() )
        emit resultsReady(output);
}
