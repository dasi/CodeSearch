#include "indexedit.h"
#include "ui_indexedit.h"

#include "indexer.h"
#include "paths.hpp"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QToolTip>

IndexEdit::IndexEdit(JobManager& jobM, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IndexEdit),
    mode_(eCreateIndex),
    jobm_(jobM)
{
    ui->setupUi(this);
    setWindowTitle(tr("Create Index"));

    connect(ui->pbAdd, &QPushButton::clicked, [this]() {
       QString path = QDir::toNativeSeparators(ui->lePath->text());
       if ( path.isNull() || path.isEmpty() )
           return;

       // check tilde
       if ( path.startsWith("~/") )
           path = QDir::homePath() + path.mid(1);

       if ( !QDir(path).exists() ) {
           QToolTip::showText(
                       ui->lePath->mapToGlobal(ui->lePath->pos()),
                       tr("<p style='color:#ff0000'>This path does not exists!</p>"
                          "<p style='color:#ffff00'>Please insert a valid file or directory.</p>"),
                       this,
                       this->rect(),
                       2000);
       } else if ( ui->lwPaths->findItems(path, Qt::MatchExactly).count() != 0 ) {
           QToolTip::showText(
                       ui->lePath->mapToGlobal(ui->lePath->pos()),
                       tr("<p style='color:#ff0000'>You already added this path!</p>"),
                       this,
                       this->rect(),
                       2000);
       } else {
           ui->lwPaths->addItem(path);
       }
    });

    connect(ui->pbClear, SIGNAL(clicked(bool)), ui->lwPaths, SLOT(clear()));
    connect(ui->pbRemove, &QPushButton::clicked, [this]() {
       auto items = ui->lwPaths->selectedItems();
       foreach(auto item, items) {
           delete item;
       }
    });

    //connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    //connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
}

IndexEdit::~IndexEdit()
{
    delete ui;
}

void IndexEdit::accept()
{
    if ( ui->leName->text().isEmpty() ) {
       QToolTip::showText(
                   ui->leName->mapToGlobal(ui->leName->pos()),
                   tr("<p style='color:#ff0000'>You need to specify a name!</p>"),
                   this,
                   this->rect(),
                   2000);
    } else if (ui->lwPaths->count() == 0) {
       QToolTip::showText(
                   ui->leName->mapToGlobal(ui->leName->pos()),
                   tr("<p style='color:#ff0000'>You need to insert at least one directory!</p>"),
                   this,
                   this->rect(),
                   2000);
    } else {
        if ( mode_ == eEditIndex )
            modifyIndex();
        else
            createIndex();
        QDialog::accept();
    }
}

void IndexEdit::setEditMode(QString index)
{
    QString indexName = QFileInfo(index).fileName();

    mode_ = eEditIndex;
    ui->leName->setReadOnly(true);
    ui->leName->setText(indexName);

    Indexer* ix = new Indexer(index);
    connect(ix, &Indexer::completed, [ix, this]() {
        foreach(auto path, ix->paths())
            ui->lwPaths->addItem(path);
        ix->deleteLater();
    });
    ix->listPaths();

    setWindowTitle(tr("Edit Index"));
}

void IndexEdit::createIndex()
{
    QString path = paths::index_storage().absoluteFilePath(ui->leName->text());
    QStringList index_paths;
    for( int i = 0; i < ui->lwPaths->count(); i++ )
        index_paths << ui->lwPaths->item(i)->text();

    auto job = [this, path, index_paths](JobManager* jobM) {
        Indexer* ix = new Indexer(path);
        QObject::connect(ix, SIGNAL(completed(QString)), jobM, SLOT(finalizeCurrentJob()));
        QObject::connect(ix, SIGNAL(completed(QString)), ix, SLOT(deleteLater()));
        ix->create(index_paths);
    };

    jobm_.get().push(QString("Creating index %1").arg(ui->leName->text()), job);
    qDebug() << "Pushed create job";
}

void IndexEdit::modifyIndex()
{
    QString path = paths::index_storage().absoluteFilePath(ui->leName->text());
    QStringList index_paths;
    for( int i = 0; i < ui->lwPaths->count(); i++ )
        index_paths << ui->lwPaths->item(i)->text();

    auto job = [this, path, index_paths](JobManager* jobM) {
        Indexer* ix = new Indexer(path);
        QObject::connect(ix, SIGNAL(completed(QString)), jobM, SLOT(finalizeCurrentJob()));
        QObject::connect(ix, SIGNAL(completed(QString)), ix, SLOT(deleteLater()));
        ix->update(index_paths);
    };

    jobm_.get().push(QString("Modifying index %1").arg(ui->leName->text()), job);
}
