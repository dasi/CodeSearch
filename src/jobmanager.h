#ifndef JOBMANAGER_H
#define JOBMANAGER_H

#include <QObject>
#include <QQueue>
#include <utility>
#include <functional>

class JobManager : public QObject
{
    Q_OBJECT
public:
    using job_function = std::function<void(JobManager*)>;
    using job_item = std::pair<QString, job_function>;


    explicit JobManager(QObject* parent = 0);

    int count() { return queue_.count(); }

    bool isEmpty() { return queue_.isEmpty(); }

    void push(QString name, job_function && f);

    void runNext();

public slots:
    void finalizeCurrentJob();

signals:
    void jobAdded();
    void jobCompleted();
    void jobStarted(QString);

private:
    QQueue<job_item> queue_;
};

#endif // JOBMANAGER_H
