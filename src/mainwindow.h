#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QCloseEvent>
#include <QMainWindow>
#include <QLabel>
#include <QAbstractTableModel>
#include "settings.h"
#include "jobmanager.h"
#include "searchengine.h"

namespace Ui {
    class MainWindow;
}

struct ResultRecord;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void searchCode();
    void loadFile(ResultRecord const & item);
    void loadIndices();
    void displayJobCount();
    void openInEditor(QModelIndex index);
    void openInEditor();
    void applyStylesheet();

protected:
    void closeEvent(QCloseEvent* ev);

private:
    Ui::MainWindow *ui;
    QScopedPointer<QAbstractTableModel> resultModel;

    // statusbar widgets
    QLabel* jobCount;

    // flag for indexing
    bool indexerRunning;

    SettingsModel cfg;
    int markerNr;
    int indicatorNr;

    JobManager jobManager;

    // store last search arguments
    SearchEngine::SearchArguments searchArgs;
};

#endif // MAINWINDOW_H
