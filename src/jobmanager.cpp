#include "jobmanager.h"

JobManager::JobManager(QObject *parent) : QObject(parent)
{

}

void JobManager::runNext() {
    auto& job = queue_.back();
    emit jobStarted(job.first);
    job.second(this);
}

void JobManager::push(QString name, job_function &&f)
{
    queue_.push_back(std::make_pair(name, f));

    emit jobAdded();

    if ( queue_.count() == 1 )
        runNext();
}

void JobManager::finalizeCurrentJob()
{
    auto job_ = queue_.dequeue();
    emit jobCompleted();
}
