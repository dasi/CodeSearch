#ifndef INDEXER_H
#define INDEXER_H

#include <QObject>
#include <QString>

class Indexer : public QObject
{    
    Q_OBJECT
public:  
    explicit Indexer(QString const &path);

    QString     name      () const { return name_;  }
    QString     indexpath () const { return path_;  }
    QStringList paths     () const { return paths_; }

public slots:
    void create(QStringList const &paths);
    void update();
    void update(QStringList const &paths);
    void remove();
    void listPaths();

signals:
    void completed(QString);

private:
    QString  path_;
    QString  name_;
    QStringList paths_;
};

#endif // INDEXER_H
