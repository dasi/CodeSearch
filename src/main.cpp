#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication::setApplicationDisplayName("Code Search");
    QApplication::setApplicationName("CodeSearch");
    QApplication::setApplicationVersion("1.1.0");
    QApplication::setDesktopSettingsAware(true);


    QApplication::setEffectEnabled(Qt::UI_General, true);
    QApplication::setEffectEnabled(Qt::UI_AnimateMenu, true);
    QApplication::setEffectEnabled(Qt::UI_FadeMenu, true);
    QApplication::setEffectEnabled(Qt::UI_AnimateCombo, true);
    QApplication::setEffectEnabled(Qt::UI_AnimateTooltip, true);
    QApplication::setEffectEnabled(Qt::UI_FadeTooltip, true);

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
