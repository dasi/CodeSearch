#ifndef INDEXEDIT_H
#define INDEXEDIT_H

#include <utility>
#include <QDialog>
#include "jobmanager.h"

namespace Ui {
class IndexEdit;
}

class IndexEdit : public QDialog
{
    Q_OBJECT

public:

    enum EditMode {
        eCreateIndex,
        eEditIndex
    };

    explicit IndexEdit(JobManager &jobM, QWidget *parent = 0);
    ~IndexEdit();

    void setEditMode(QString index);

protected slots:
    void accept() override;

private slots:
    void createIndex();
    void modifyIndex();

private:
    Ui::IndexEdit *ui;
    EditMode mode_;
    std::reference_wrapper<JobManager> jobm_;
};

#endif // INDEXEDIT_H
