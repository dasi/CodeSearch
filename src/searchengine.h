#ifndef SEARCHENGINE_H
#define SEARCHENGINE_H

#include <QObject>
#include <QProcess>
#include "qresultviewmodel.h"

class SearchEngine : public QObject
{
    Q_OBJECT
public:

    struct SearchArguments {
        QString indexPath;
        QString searchRx;
        QString pathRx;
        bool caseInsensitive;
        int maxResults;
        int maxResultPerFile;
    };

    explicit SearchEngine(QObject *parent = 0);

signals:
    void resultsReady(QList<ResultRecord> result);
    void completed();

public slots:
    /*void search(QString indexPath, QString searchRx);
    void search(QString indexPath, QString searchRx, QString pathRx);
    void search(QString indexPath, QString searchRx, QString pathRx, bool caseInsensitive);*/
    void search(SearchArguments const & args);

private slots:
    void parseCSearchResult();

private:
    void spawnCSearch(QStringList const & args);
    QProcess searcher;
};

#endif // SEARCHENGINE_H
