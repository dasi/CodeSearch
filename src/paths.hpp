#ifndef PATHS_HPP
#define PATHS_HPP

#include <QString>
#include <QDir>

namespace paths {
    inline QDir app_storage() {
        auto home_ = QDir::home();
        // create path if not exists
        home_.mkdir(".codesearch");
        if (  home_.cd(".codesearch") )
            return home_;
        return QDir::current();
    }

    inline QDir index_storage() {
        auto app_ = app_storage();
        app_.mkdir("indices");
        if ( app_.cd("indices") )
            return app_;
        return app_storage();
    }

    inline QDir theme_directory() {
        auto app_ = app_storage();
        app_.mkdir("theme");
        if ( app_.cd("theme") )
            return app_;
        return app_storage();
    }

    inline QFileInfo settings_file() {
        return QFileInfo(app_storage().absoluteFilePath("settings.json"));
    }
}

#endif // PATHS_HPP
