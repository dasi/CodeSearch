#include "qresultviewmodel.h"
#include <QDebug>


enum model_columns {
    c_name,
    c_fext,
    c_line,
    c_path
};

QResultViewModel::QResultViewModel()
{

}

QResultViewModel::QResultViewModel(const QList<ResultRecord> &result)
    : _result(result)
{

}

// QAbstractTableModel overloads
int QResultViewModel::rowCount(const QModelIndex &parent) const
{
    return _result.length();
}

int QResultViewModel::columnCount(const QModelIndex &parent) const
{
    // fixed by ResultRecord fields
    return 4;
}

QVariant QResultViewModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role);

    auto& item = _result[index.row()];

    auto get_cell_value = [&]() -> QVariant {
        switch(index.column()) {
        case c_name: return item.file.fileName();
        case c_line: return item.rowNr;
        case c_fext: return item.file.suffix();
        case c_path: return item.file.absolutePath();
        default: return QVariant();
        };
    };

    switch(role)  {
    case Qt::DisplayRole: return get_cell_value();
    case Qt::DecorationRole:
        return index.column() == 0 ? _fileIconProvider.icon(item.file) : QVariant();
    case Qt::UserRole: return QVariant::fromValue(item);
    default: return QVariant();
    };
}

Qt::ItemFlags QResultViewModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QVariant QResultViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if ( role == Qt::DisplayRole && orientation == Qt::Horizontal ) {
        switch(section) {
        case c_name: return tr("File name");
        case c_fext: return tr("Extension");
        case c_line: return tr("Line");
        case c_path: return tr("Full path");
        default: return "";
        };
    } else if ( role == Qt::DisplayRole ){
        return QString::number(section+1);
    } else {
        return QVariant();
    }
}

void QResultViewModel::merge(QList<ResultRecord> const & items)
{
    //auto topLeft = index(_result.count(), 0);
    _result.append(items);
    //auto botRight = index(_result.count(), 0);
    //emit dataChanged(topLeft, botRight);
    emit layoutChanged();

    //qDebug() << "result found: " << _result.count();
}
