# CodeSearch

CodeSearch is a nice GUI on top of [google/codesearch](https://github.com/junkblocker/codesearch) command line tools.



It looks like this on Windows 7:

![Windows 7 Screenshot](assets/repo/win7_main_view.png)

and like this on Ubuntu:

![Ubuntu 16.04 Screenshot](assets/repo/ubuntu_main_view.png)

