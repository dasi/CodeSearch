# - Try to find the QScintilla2 includes and library
# which defines
#
# QSCINTILLA_FOUND - system has QScintilla2
# QSCINTILLA_INCLUDE_DIR - where to find qextscintilla.h
# QSCINTILLA_LIBRARIES - the libraries to link against to use QScintilla
# QSCINTILLA_LIBRARY - where to find the QScintilla library (not for general use)

# copyright (c) 2007 Thomas Moenicke thomas.moenicke@kdemail.net
#               2009 Petr Vanek petr@scribus.info
#
# Redistribution and use is allowed according to the terms of the FreeBSD license.

SET(QSCINTILLA_FOUND FALSE)

IF(Qt5Widgets_FOUND)
    FOREACH(TOP_INCLUDE_PATH ${Qt5Widgets_INCLUDE_DIRS} ${FRAMEWORK_INCLUDE_DIR})
        FIND_PATH(QSCINTILLA_INCLUDE_DIR qsciglobal.h ${TOP_INCLUDE_PATH}/Qsci)

        IF(QSCINTILLA_INCLUDE_DIR)
            BREAK()
        ENDIF()
    ENDFOREACH()

    GET_TARGET_PROPERTY(QT5_WIDGETSLIBRARY Qt5::Widgets LOCATION)
    GET_FILENAME_COMPONENT(QT5_WIDGETSLIBRARYPATH ${QT5_WIDGETSLIBRARY} PATH)

    SET(QSCINTILLA_NAMES qt5scintilla2d qscintilla2_qt5d)
    FIND_LIBRARY(QSCINTILLA_LIBRARY_DEBUG
        NAMES ${QSCINTILLA_NAMES}
        PATHS ${QT5_WIDGETSLIBRARYPATH})

    SET(QSCINTILLA_NAMES qt5scintilla2 qscintilla2_qt5)
    FIND_LIBRARY(QSCINTILLA_LIBRARY_RELEASE
        NAMES ${QSCINTILLA_NAMES}
        PATHS ${QT5_WIDGETSLIBRARYPATH})

ENDIF()

IF (QSCINTILLA_LIBRARY_RELEASE AND QSCINTILLA_INCLUDE_DIR)
    IF(CMAKE_BUILD_TYPE MATCHES Debug)
        SET(QSCINTILLA_LIBRARIES ${QSCINTILLA_LIBRARY_DEBUG})
    ELSE()
        SET(QSCINTILLA_LIBRARIES ${QSCINTILLA_LIBRARY_RELEASE})
    ENDIF()
    SET(QSCINTILLA_FOUND TRUE)

ENDIF (QSCINTILLA_LIBRARY_RELEASE AND QSCINTILLA_INCLUDE_DIR)

IF (QSCINTILLA_FOUND)
  IF (NOT QScintilla_FIND_QUIETLY)
    MESSAGE(STATUS "Found QScintilla2: ${QSCINTILLA_LIBRARY}")
    MESSAGE(STATUS "         includes: ${QSCINTILLA_INCLUDE_DIR}")
  ENDIF (NOT QScintilla_FIND_QUIETLY)
ELSE (QSCINTILLA_FOUND)
  IF (QScintilla_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find QScintilla library")
  ENDIF (QScintilla_FIND_REQUIRED)
ENDIF (QSCINTILLA_FOUND)

MARK_AS_ADVANCED(QSCINTILLA_INCLUDE_DIR QSCINTILLA_LIBRARY)
